import React from 'react';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { toast } from 'react-toastify';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import Autosuggest from 'react-autosuggest';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import TextField from '@material-ui/core/TextField';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import fileProvider from '../../data-access/file';
import contractProvider from "../../data-access/contract";
import userProvider from '../../data-access/user';

function Transition(props) {
    return <Slide direction="up" {...props} />;
}


class ModalAddContact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: true,
            value: '',
            suggestions: [],
            type: -1,
            code: '',
            userSelected:''
        };
        console.log(this.props.user)
    }

    handleClose = () => {
        this.props.callbackOff()
    };

    save = () => {
        const { code, file, userSelected, type, dateContract, dateEndContract, dateStartContract } = this.state
        let param = {
            contract: {
                code,
                contractAttach: file,
                dateContract: moment(dateContract).format('YYYY-MM-DD'),
                dateEndContract: moment(dateEndContract).format('YYYY-MM-DD'),
                dateStartContract: moment(dateStartContract).format('YYYY-MM-DD')
            },
            contractTypeId: type,
        }
        contractProvider.create(param, (s, e) => {
            console.log(s, e)
            if (s && s.code == 0) {
                this.updateEmployee(s)
            } else {

            }
        });
    }

    updateEmployee(s) {
        const {userSelected} = this.state
        console.log("--------------------------------------------")
        console.log(userSelected)
        console.log("--------------------------------------------")
        let param = {
            id: userSelected.employees.id,
            employees: {
                name: userSelected.employees.name,
                mail: userSelected.employees.mail,
                phoneNumber: userSelected.employees.phoneNumber,
                isofhMail: userSelected.employees.isofhMail,
                "birthDay": moment(userSelected.employees.birthDay).format('YYYY-MM-DD'),
                "specialize": userSelected.employees.specialize,
                cv:userSelected.employees.cv,
                evaluate: userSelected.employees.evaluate,
                image: userSelected.employees.image,
                salaryProposed: userSelected.employees.salaryProposed,
                dayRemaining: userSelected.employees.dayRemaining,
                gender: userSelected.employees.gender,
                university: userSelected.employees.university,
                majors: userSelected.employees.majors,
                currentAddress: userSelected.employees.currentAddress,
                cmt: userSelected.employees.cmt,
                place: userSelected.employees.place,
                dateRange: moment(userSelected.employees.dateRange).format('YYYY-MM-DD'),
                normalAddress: userSelected.employees.normalAddress,
                bankAccount: userSelected.employees.bankAccount,
                codePersonal: userSelected.employees.codePersonal
            },
            contractId:s.data.contract.id,
            departmentId: userSelected.department.id,
            recruitmentSourcesId: userSelected.recruitmentSources.id,
        }
        userProvider.update(param, (s, e) => {
            console.log(s)
            if (s && s.data) {
                // this.updateUserAfterCreate(s.data.employees, startDate, positionSelected)
                this.handleClose()
            } else {
                toast.error(s.message, {
                    position: toast.POSITION.TOP_CENTER
                });
            }
        })
    }

    renderSuggestion = suggestion => (
        <div onClick={() => this.setState({ userSelected: suggestion })} style={{ display: 'flex', padding: 5 }}>
            {suggestion.employees.image ?
                <Avatar alt="Remy Sharp" src={suggestion.employees.image.absoluteUrl()} />
                :
                <Avatar alt="Remy Sharp" src="/assets/avatars/1.jpg" />
            }
            <div style={{ paddingLeft: 5 }}>
                <h6 style={{ margin: 0 }}>{suggestion.employees.name}</h6>
                <p style={{ margin: 0 }}>{suggestion.department ? suggestion.department.name : ''} - {suggestion.specialize ? suggestion.specialize.name : ''}</p>
            </div>
        </div>
    );

    onChange = (event, { newValue }) => {
        this.setState({ value: newValue });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    getSuggestions = value => {
        const users = this.props.users
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        return inputLength === 0 ? [] : users.filter(item => item.employees.name.toLowerCase().slice(0, inputLength) === inputValue);
    };

    getSuggestionValue = suggestion => suggestion.employees.name

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    renderInputComponent = inputProps => (
        <div>
            <TextField
                id="filled-name"
                label="Họ và tên nhân viên"
                {...inputProps}
                margin="normal"
            />
        </div>
    )

    validateFile = (event) => {
        fileProvider.uploadFile(event.target.files[0], (res) => {
            console.log(res)
            if (res && res.data && res.data.code == 0) {
                this.setState({
                    file: res.data.data.File.fileName
                })
            } else {
                toast.error("Vui lòng thử lại !", {
                    position: toast.POSITION.TOP_LEFT
                });
            }
        })
    }
    render() {
        const { dataselect, classes, contractType } = this.props;
        const { value, suggestions, code, type, dateContract, dateEndContract, dateStartContract } = this.state;
        const inputProps = {
            placeholder: 'Họ và tên nhân viên',
            value,
            onChange: this.onChange
        };
        return (
            <Dialog
                open={this.state.open}
                TransitionComponent={Transition}
                keepMounted
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description">
                <DialogTitle style={{ textAlign: 'center' }} id="alert-dialog-slide-title">{dataselect ? 'Cập nhật' : 'Thêm mới'}</DialogTitle>
                <DialogContent>
                    <Grid container spacing={16}>
                        <Grid item xs={12} md={6}>
                            <Autosuggest
                                suggestions={suggestions}
                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                getSuggestionValue={this.getSuggestionValue}
                                renderSuggestion={this.renderSuggestion}
                                inputProps={inputProps}
                                renderInputComponent={this.renderInputComponent}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField
                                style={{ width: '100%' }}
                                value={code}
                                id="filled-name"
                                label="Số hợp đồng*"
                                className={classes.textField}
                                onChange={(event) => this.setState({ code: event.target.value })}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <InputLabel htmlFor="age-helper">Loại hợp đồng</InputLabel>
                            <Select
                                value={type}
                                onChange={(event) => this.setState({ type: event.target.value })}
                                inputProps={{ name: 'selectDepartment', id: 'selectDepartment' }}
                                style={{ width: '100%', marginTop: 8 }}>
                                {contractType.map((item, idx) => (
                                    <MenuItem key={idx} value={item.contract ? item.contract.id : ''}>{item.contract ? item.contract.name : ''}</MenuItem>
                                ))}
                            </Select>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <div style={{ marginBottom: '16px' }}></div>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <DatePicker
                                    value={dateContract}
                                    label="Ngày ký"
                                    onChange={(date) => this.setState({ dateContract: date })}
                                    leftArrowIcon={<KeyboardArrowLeft />}
                                    rightArrowIcon={<KeyboardArrowRight />}
                                    labelFunc={date => (date ? moment(date).format('DD-MM-YYYY') : '')}
                                    style={{ width: '100%' }}
                                    maxDate={new Date()}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <div style={{ marginBottom: '16px' }}></div>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <DatePicker
                                    value={dateStartContract}
                                    label="Ngày bắt đầu hợp đồng"
                                    onChange={(date) => this.setState({ dateStartContract: date })}
                                    leftArrowIcon={<KeyboardArrowLeft />}
                                    rightArrowIcon={<KeyboardArrowRight />}
                                    labelFunc={date => (date ? moment(date).format('DD-MM-YYYY') : '')}
                                    style={{ width: '100%' }}
                                    maxDate={new Date()}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <div style={{ marginBottom: '16px' }}></div>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <DatePicker
                                    value={dateEndContract}
                                    label="Ngày kết thúc hợp đồng"
                                    onChange={(date) => this.setState({ dateEndContract: date })}
                                    leftArrowIcon={<KeyboardArrowLeft />}
                                    rightArrowIcon={<KeyboardArrowRight />}
                                    labelFunc={date => (date ? moment(date).format('DD-MM-YYYY') : '')}
                                    style={{ width: '100%' }}
                                    maxDate={new Date()}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} md={6} style={{ textAlign: 'center', lineHeight: 5 }}>
                            <input
                                accept=".doc,.docx,.ppt,.pptx"
                                className={classes.input}
                                style={{ display: 'none' }}
                                id="raised-button-file"
                                onChange={this.validateFile}
                                type="file"
                            />
                            <label htmlFor="raised-button-file">
                                <Button variant="contained" component="span" className={classes.button}>File hợp đồng đính kèm</Button>
                            </label>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} variant="contained" color="secondary">Hủy</Button>
                    <Button onClick={this.save} variant="contained" color="primary">Lưu</Button>
                </DialogActions>
            </Dialog>
        );
    }
}
const styles = {
    row: {
        display: 'flex',
        justifyContent: 'center',
    },
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 60,
        height: 60,
    },
};
export default withStyles(styles)(ModalAddContact);