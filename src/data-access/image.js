import client from '../utils/client-utils';
import API from '../config/api';
export default {
    upload(image, cb) {
        client.uploadFile(API.image.upload, image, cb).then((s, e) => {
            s ? cb(s) : cb(e)
        })
    },
}