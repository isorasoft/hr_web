import React from 'react';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { toast } from 'react-toastify';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import contractTypeProvider from "../../data-access/contract_type";

function Transition(props) {
    return <Slide direction="up" {...props} />;
}


class ModalAddTypeContact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: true,
            name: this.props.dataselect && this.props.dataselect.name ? this.props.dataselect.name : '',
            note: this.props.dataselect && this.props.dataselect.note ? this.props.dataselect.note : ''
        };
    }

    handleClose = () => {
        this.props.callbackOff()
    };

    save = () => {
        const { name, note } = this.state
        let param = {
            id: this.props.dataselect ? this.props.dataselect.id : '',
            contractType: {
                name,
                note
            }
        }
        if (name === "") {
            alert("Loại hợp đồng không được để trống")
        } else {
            if (this.props.dataselect) {
                contractTypeProvider.update(param, (s, e) => {
                    if (s.code != 0) {
                        toast.error(s.message, {
                            position: toast.POSITION.TOP_RIGHT
                        });
                    } else {
                        toast.success("Cập nhật thành công !", {
                            position: toast.POSITION.TOP_CENTER
                        });
                        this.handleClose()
                    }
                });
            } else {
                contractTypeProvider.create(param, (s, e) => {
                    if (s.code != 0) {
                        toast.error(s.message, {
                            position: toast.POSITION.TOP_RIGHT
                        });
                    } else {
                        toast.success("Tạo mới thành công !", {
                            position: toast.POSITION.TOP_CENTER
                        });
                        this.handleClose()
                    }
                });
            }
        }
    }

    render() {
        const { dataselect, classes } = this.props;
        const { open, name, note } = this.state;
        return (
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description">
                <DialogTitle style={{ textAlign: 'center' }} id="alert-dialog-slide-title">{dataselect ? 'Cập nhật' : 'Thêm mới'}</DialogTitle>
                <DialogContent>
                    <Grid container spacing={16}>
                        <Grid item xs={12} md={6}>
                            <TextField
                                style={{ width: '100%' }}
                                value={name}
                                id="filled-name"
                                label="Tên loại*"
                                className={classes.textField}
                                onChange={(event) => this.setState({ name: event.target.value })}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField
                                style={{ width: '100%' }}
                                value={note}
                                id="filled-name"
                                label="Ghi chú"
                                className={classes.textField}
                                onChange={(event) => this.setState({ note: event.target.value })}
                                margin="normal"
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} variant="contained" color="secondary">Hủy</Button>
                    <Button onClick={this.save} variant="contained" color="primary">Lưu</Button>
                </DialogActions>
            </Dialog>
        );
    }
}
const styles = {
    row: {
        display: 'flex',
        justifyContent: 'center',
    },
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 60,
        height: 60,
    },
};
export default withStyles(styles)(ModalAddTypeContact);