import React, { Component } from 'react'
import moment from 'moment';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ToastContainer, toast } from 'react-toastify';
import Paper from '@material-ui/core/Paper';
import classNames from 'classnames';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import Tooltip from '@material-ui/core/Tooltip';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import EditIcon from '@material-ui/icons/Edit';

import ModalAddTypeContact from './add_type_contract';
import EnhancedTableToolbar from '../../components/table-toolbar';
import TablePaginationActions from '../../components/pagination/pagination';
import contractTypeProvider from "../../data-access/contract_type";

class ContractType extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            progress: true,
            data: [],
            page: 0,
            size: 10,
            total: 100,
            modalAdd: false
        }
    }

    getData() {
        contractTypeProvider.search({}, (s, e) => {
            if (s && s.data) {
                console.log(s)
                this.setState({ 
                    data:s.data.data,
                    progress: false 
                })
            } else {
            }
        })
    }

    componentWillMount() {}

    componentDidMount() {
        this.getData()
    }

    createUpdate(type) {
        if (type) {
            this.setState({
                modalAdd: true,
                typeEdit: type
            })
        } else {
            this.setState({
                modalAdd: true,
                typeEdit: null
            })
        }
    }
    renderChirenToolbar() {
        return (
            <Button variant="contained" size="small" color="primary" style={{ marginLeft: 20, marginTop: 10 }} onClick={() => this.createUpdate(null)}>
                Thêm mới <CheckIcon style={{ marginLeft: 10 }} />
            </Button>
        )
    }

    handleChangePage = (event, action) => {
        this.setState({
            page: action,
        }, () => {
            console.log(this.state.page)
            this.getData()
        });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ size: event.target.value }, () => {
            this.getData()
        });
    };

    closeModal() {
        this.getData()
        this.setState({ modalAdd: false });
    }
    render() {
        const { classes } = this.props;
        const { progress, data, page, size, total, modalAdd, typeEdit } = this.state;
        return (
            <div>
                <ToastContainer autoClose={3000} />
                <Paper className={classes.root}>
                    <div className={classes.tableWrapper}>
                        <EnhancedTableToolbar
                            numSelected={0}
                            title="Loại hợp đồng"
                            actionsChiren={
                                this.renderChirenToolbar()
                            }
                        />
                        {progress ? <LinearProgress /> : null}
                        {data && data.length ?
                            <Table className={classes.table} aria-labelledby="tableTitle">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Loại hợp đồng</TableCell>
                                        <TableCell>Ghi chú</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {data ? data.map(item => {
                                        return (
                                            <TableRow
                                                hover
                                                key={item.contract.id}
                                                tabIndex={-1}>
                                                <TableCell>{item.contract.name}</TableCell>
                                                <TableCell>{item.contract.note}</TableCell>
                                                <TableCell>
                                                    <IconButton
                                                        onClick={() => this.createUpdate(item.contract)}
                                                        size="small"
                                                        color="primary"
                                                        className={classes.button} aria-label="Edit">
                                                        <EditIcon />
                                                    </IconButton>

                                                </TableCell>
                                            </TableRow>
                                        );
                                    }) : null}
                                </TableBody>
                                {/* <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            labelRowsPerPage="Số dòng trên trang"
                                            rowsPerPageOptions={[10, 20, 50, 100]}
                                            colSpan={3}
                                            count={total}
                                            rowsPerPage={size}
                                            page={page}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                            ActionsComponent={TablePaginationActions}
                                        />
                                    </TableRow>
                                </TableFooter> */}
                            </Table>
                            : <p>Danh sách trống</p>
                        }
                    </div>
                </Paper>
                {
                    modalAdd && <ModalAddTypeContact dataselect={typeEdit} callbackOff={this.closeModal.bind(this)} />
                }
            </div>
        )
    }
}
const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 1020,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

ContractType.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContractType);