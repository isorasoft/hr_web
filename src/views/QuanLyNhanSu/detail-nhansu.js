import React from 'react';
import { toast } from 'react-toastify';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import moment from 'moment';
import { Col, Row } from 'reactstrap';


function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DetailUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: true,
            dataUser: this.props.data,
            listGender: this.props.gender,
            image: this.props.data && this.props.data.employees && this.props.data.employees.image ? this.props.data.employees.image : '',
            name: this.props.data && this.props.data.employees ? this.props.data.employees.name : '',
            gender: this.props.data.employees ? this.props.data.employees.gender : '',
            birthDay: this.props.data.employees ? this.props.data.employees.birthDay : '',
            mail: this.props.data && this.props.data.employees ? this.props.data.employees.mail : '',
            isofhMail: this.props.data && this.props.data.employees ? this.props.data.employees.isofhMail : '',
            phoneNumber: this.props.data && this.props.data.employees ? this.props.data.employees.phoneNumber : '',
            cmt: this.props.data && this.props.data.employees ? this.props.data.employees.cmt : '',
            dateRange: this.props.data && this.props.data.employees ? this.props.data.employees.dateRange : '',
            place: this.props.data && this.props.data.employees ? this.props.data.employees.place : '',
            normalAddress: this.props.data && this.props.data.employees ? this.props.data.employees.normalAddress : '',
            currentAddress: this.props.data && this.props.data.employees ? this.props.data.employees.currentAddress : '',
            university: this.props.data && this.props.data.employees ? this.props.data.employees.university : '',
            majors: this.props.data && this.props.data.employees ? this.props.data.employees.majors : '',
            codePersonal: this.props.data && this.props.data.employees ? this.props.data.employees.codePersonal : '',
            bankAccount: this.props.data && this.props.data.employees ? this.props.data.employees.bankAccount : '',
            username: this.props.data && this.props.data.employees ? this.props.data.employees.username : '',
        };

    }

    handleClose = () => {
        this.props.callbackOff()
    };

    render() {
        const { classes } = this.props;
        const { dataUser, listGender, image, name, gender, birthDay, mail, isofhMail, phoneNumber, cmt, dateRange, place, normalAddress, currentAddress, university, majors, codePersonal, bankAccount, username } = this.state;
        return (
            <div style={{ backgroundColor: 'red' }}>
                <Dialog
                    open={this.state.open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={this.handleClose}
                    fullWidth="md"
                    maxWidth="md"
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description">
                    <DialogTitle id="alert-dialog-slide-title">
                        Chi tiết tài khoản người dùng
                    </DialogTitle>
                    <DialogContent>
                        <Row style={{ marginLeft: 40 }}>
                            <Col xs="12" sm="3" md="3">
                                <div class="row mgbt-xs-0">
                                    <Avatar
                                        style={{ width: 100, height: 100 }} alt="Remy Sharp"
                                        src={image ? image.absoluteUrl() : "/assets/avatars/1.jpg"}
                                        className={classNames(classes.avatar, classes.bigAvatar)} />
                                </div>
                            </Col>
                            <Col xs="12" sm="5" md="5">
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Họ tên:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{name}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Giới tính:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{gender == 1 ? 'Nam' : gender == 2 ? 'Nữ' : ''}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Ngày sinh:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{moment(birthDay).format("DD-MM-YYYY")}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Email:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{mail}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>iSofH mail:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{isofhMail}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Trường:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{university}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Chuyên ngành:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{majors}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Mã nhân viên:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{codePersonal}</div>
                                </div>
                            </Col>
                            <Col xs="12" sm="4" md="4">
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Số điện thoại:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{phoneNumber}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>CNMD:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{cmt}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Ngày cấp:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{moment(dateRange).format("DD-MM-YYYY")}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Nơi cấp:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{place}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Địa chỉ hiện tại:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{normalAddress}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Địa chỉ thường chú:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{currentAddress}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Tài khoản ngân hàng:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{bankAccount}</div>
                                </div>
                                <div class="row mgbt-xs-0">
                                    <label class="col-xs-5 control-label" className={classes.controlLabel}>Username:</label>
                                    <div class="col-xs-7 controls" className={classes.controls}>{username}</div>
                                </div>
                            </Col>

                        </Row>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} variant="contained" color="inherit">Cancel</Button>
                    </DialogActions>
                </Dialog>
            </div >
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.currentUser
    };
}

const styles = theme => ({
    row: {
        display: 'flex',
        justifyContent: 'center',
    }, textField: {
        width: '100%'
    }, avatar: {
        margin: 10,
    }, bigAvatar: {
        width: 60,
        height: 60,
    }, controlLabel: {
        fontWeight: 'bold',
        width: 145,
        marginTop: 10,
        marginBottom: 20,
    }, controls: {
        marginTop: 10,
    }, labelAvatar: {
        fontWeight: 'bold',
        width: 100,
        marginTop: 7,
    }, controlsAvatar: {
        marginTop: 7,
    }
});

export default withStyles(styles)(DetailUser);