import client from '../utils/client-utils';
import API from '../config/api';
export default {
    search(param, cb) {
        let parameters =
            (param.page ? '?page=' + param.page : '?page=' + -1) +
            (param.size ? '&size=' + param.size : '&size=' + - 1) +
            (param.note ? '&note=' + param.note : '') +
            (param.title ? '&title=' + param.title : '') +
            (param.id ? '&id=' + param.id : '') +
            (param.employeesId ? '&employeesId=' + param.employeesId : '') +
            (param.status ? '&status=' + param.status : '') +
            (param.departmentId ? '&departmentId=' + param.departmentId : '')
        client.requestApi("get", API.notification.search + parameters, {}, (s, e) => {
            if (cb) {
                cb(s, e);
            }
        })
    },
    create(param, cb) {
        client.requestApi("POST", API.notification.create, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
    createNews(param, cb) {
        client.requestApi("POST", API.notification.createNews, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
    delete(id, param, cb) {
        client.requestApi("DELETE", API.notification.delete + "/" + id, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
    update(param, cb) {
        client.requestApi("PUT", API.notification.update + "/" + param.id, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
}