import React, { Component } from 'react';
import moment from 'moment';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Card, CardBody, CardFooter, CardHeader, Col, Row, Collapse, Fade } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import Button from '@material-ui/core/Button';
import "react-daterange-picker/dist/css/react-calendar.css";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePaginationActions from '../../components/pagination/pagination';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import LinearProgress from '@material-ui/core/LinearProgress';
import EnhancedTableToolbar from '../../components/table-toolbar';
import ConfirmDialog from '../../components/confirm/'
import departmentProvider from '../../data-access/department';
import notificationProvider from '../../data-access/notification';
import imageProvider from '../../data-access/image';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import ClientUtils from '../../utils/client-utils'

class News extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 0,
            size: 10,
            status: 1,
            data: [],
            total: 0,
            stt: 0,
            title: '',
            value: '',
            images: '',
            startActive: new Date(),
            listDepartment: [],
            confirmDialog: false,
            tempDelete: {},
            idUpdate: '',
        }
    }

    componentWillMount() {
        this.getData();
        this.getDepartment();
    }

    getData() {
        this.setState({ progress: true })
        let params = {
            page: this.state.page + 1,
            size: this.state.size,
            status: this.state.status,
        }
        notificationProvider.search(params, (s, e) => {
            if (s && s.data) {
                let stt = 1 + (params.page - 1) * params.size;
                this.setState({
                    data: s.data.data,
                    total: s.data.total,
                    stt,
                })
            } else {
                toast.error("Không lấy được dữ liệu !", {
                    position: toast.POSITION.BOTTOM_CENTER
                });
            }
            this.setState({ progress: false })
        })

        this.progress();
    }

    progress = () => {
        const { completed } = this.state;
        if (completed > 100) {
            this.setState({ completed: 0, buffer: 10 });
        } else {
            const diff = Math.random() * 10;
            const diff2 = Math.random() * 10;
            this.setState({ completed: completed + diff, buffer: completed + diff + diff2 });
        }
    };

    getDepartment() {
        let param = {
            page: 1,
            size: ''
        }
        let dataTemp = [];
        departmentProvider.search(param, (s, e) => {
            if (s && s.data) {
                if (s.data && s.data.data.length > 0) {
                    for (let i = 0; i < s.data.data.length; i++) {
                        dataTemp.push(s.data.data[i])
                    }
                    this.setState({ listDepartment: dataTemp })
                }
            }
        })
    }

    validateDataSend() {
        const { title, value, images } = this.state;
        let msg = '';
        if (!title) {
            msg = msg + "Nhập tiêu đề tin tức! \n"
        }
        if (!value) {
            msg = msg + "Nhập nội dung tin tức! \n"
        }
        if (!images) {
            msg = msg + "Vui lòng chọn ảnh tin tức! \n"
        }
        return msg
    }

    uploadAvatar = (event) => {
        imageProvider.upload(event.target.files[0], (s, e) => {
            if (s && s.data && s.data.code == 0) {
                this.setState({
                    images: s.data.data.images[0].image,
                });
            } else {
                toast.error("Vui lòng thử lại !", {
                    position: toast.POSITION.TOP_LEFT
                });
            }
        })

    }

    create() {
        const { idUpdate, listDepartment, title, value, images, startActive } = this.state;
        let departmentIds = [];
        for (let i = 0; i < listDepartment.length; i++) {
            departmentIds.push(listDepartment[i].department.id)
        }
        let param = {
            id: idUpdate ? idUpdate : '',
            notification: {
                title,
                value,
                images,
                startActive: moment(startActive).format("YYYY-MM-DD")
            },
            departmentIds
        }
        if (this.validateDataSend() === '') {
            if (idUpdate) {
                notificationProvider.update(param, (s, e) => {
                    if (s && s.data) {
                        this.handleCancelData();
                        this.getData();
                        toast.success("update news success!", {
                            position: toast.POSITION.TOP_CENTER
                        });
                    } else {
                        toast.error(s.message, {
                            position: toast.POSITION.TOP_CENTER
                        });
                    }
                })
            } else {
                notificationProvider.createNews(param, (s, e) => {
                    if (s && s.data) {
                        this.handleCancelData();
                        this.setState({ page: 0 })
                        this.getData();
                        toast.success("create news success!", {
                            position: toast.POSITION.TOP_CENTER
                        });
                    } else {
                        toast.error(s.message, {
                            position: toast.POSITION.TOP_CENTER
                        });
                    }
                })
            }
        } else {
            alert(this.validateDataSend())
        }
    }

    handleCancelData() {
        this.setState({
            title: '',
            value: '',
            images: '',
            idUpdate: '',
            startActive: new Date(),
        })
    }

    handleChangePage = (event, action) => {
        this.setState({
            page: action,
            selected: []
        }, () => {
            this.getData()
        });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ size: event.target.value }, () => {
            this.getData();
        });
    };


    onSelect = (value, states) => {
        this.setState({ dateValue: value });
    };

    showDataUpdate(item) {
        this.setState({
            title: item.title,
            value: item.value,
            images: item.images,
            idUpdate: item.id,
            startActive: item.startActive
        })
    }

    showModalDelete(item) {
        this.setState({ confirmDialog: true, tempDelete: item })
    }

    delete(type) {
        this.setState({ confirmDialog: false })
        if (type == 1) {
            let param = {};
            notificationProvider.delete(this.state.tempDelete.id, param, (s, e) => {
                if (s && s.data) {
                    toast.success("Xóa tin tức thành công!", {
                        position: toast.POSITION.TOP_CENTER
                    });
                    this.getData();
                    this.setState({ tempDelete: {} });
                } else {
                    toast.error("Xóa tin tức không thành công!", {
                        position: toast.POSITION.TOP_CENTER
                    });
                }
            })
        }
    }

    renderChirenToolbar() {
        const { classes } = this.props;
        return (
            <div>

            </div>
        )
    }

    render() {
        const { classes } = this.props;
        const { data, images, title, value, startActive, stt, page, size, total, progress, confirmDialog, idUpdate } = this.state;
        return (
            <div className="animated fadeIn">
                <ToastContainer autoClose={3000} />
                <Row>
                    <Col xs="12" sm="8" md="8">
                        <Card>
                            <CardHeader>
                                <EnhancedTableToolbar
                                    title="Danh sách tin tức"
                                    numSelected={0}
                                    actionsChiren={
                                        this.renderChirenToolbar()
                                    }
                                />
                            </CardHeader>
                            <CardBody>
                                {progress ? <LinearProgress /> : null}
                                <Table className={classes.table} aria-labelledby="tableTitle">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>STT</TableCell>
                                            <TableCell>Tiêu đề</TableCell>
                                            <TableCell>Ngày hoạt động</TableCell>
                                            <TableCell>Tiện ích</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            data && data.length ? data.map((item, index) => {
                                                return (
                                                    <TableRow
                                                        hover
                                                        key={index}
                                                        tabIndex={-1}>
                                                        <TableCell>{index + stt}</TableCell>
                                                        <TableCell>{item.notification.title}</TableCell>
                                                        <TableCell>{moment(item.notification.startActive).format("DD-MM-YYYY")}</TableCell>
                                                        <TableCell>
                                                            <IconButton onClick={this.showDataUpdate.bind(this, item.notification)} color="primary" className={classes.button} aria-label="EditIcon">
                                                                <EditIcon />
                                                            </IconButton>
                                                            <IconButton onClick={this.showModalDelete.bind(this, item.notification)} color="secondary" className={classes.button} aria-label="IconButton">
                                                                <DeleteIcon />
                                                            </IconButton>

                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })
                                                :
                                                <TableRow>
                                                    <TableCell>Danh sách trống</TableCell>
                                                </TableRow>
                                        }
                                    </TableBody>
                                    <TableFooter>
                                        <TableRow>
                                            <TablePagination
                                                labelRowsPerPage="Số dòng trên trang"
                                                rowsPerPageOptions={[10, 20, 50, 100]}
                                                colSpan={3}
                                                count={total}
                                                rowsPerPage={size}
                                                page={page}
                                                onChangePage={this.handleChangePage}
                                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                                ActionsComponent={TablePaginationActions}
                                            />
                                        </TableRow>
                                    </TableFooter>
                                </Table>
                                {confirmDialog && <ConfirmDialog title="Xác nhận" content="Bạn có chắc chắn muốn xóa tin tức này không?" btnOk="Xác nhận" btnCancel="Hủy" cbFn={this.delete.bind(this)} />}
                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs="12" sm="4" md="4">
                        <Card>
                            <CardHeader>
                                <EnhancedTableToolbar
                                    title={idUpdate ? "Chỉnh sửa tin tức" : "Thêm mới tin tức"}
                                    numSelected={0}
                                    actionsChiren={
                                        this.renderChirenToolbar()
                                    }
                                />
                            </CardHeader>
                            <CardBody>
                                <div>
                                    <div style={{ textAlign: 'center' }}>
                                        <input
                                            accept="image/png"
                                            className={classes.input}
                                            style={{ display: 'none' }}
                                            id="upload_avatar"
                                            onChange={this.uploadAvatar}
                                            type="file"
                                        />
                                        <label htmlFor="upload_avatar">
                                            <Button component="span">
                                                {images ? <img style={{ width: 269, margin: 'auto', border: "1px soild" }}
                                                    src={images.absoluteUrl()} />
                                                    : <img style={{ width: 69, margin: 'auto', border: "1px soild" }}
                                                        src="/assets/image-icon.png" />}
                                            </Button>
                                        </label>
                                    </div>
                                    <input onChange={(event) => this.setState({ title: event.target.value })}
                                        style={{ width: '100%', height: '35px', borderTop: 'none', borderRight: 'none', borderLeft: 'none', borderBottom: '1px solid #a2a2a2', marginBottom: '20px' }}
                                        value={title} placeholder='Tiêu đề tin tức'></input>
                                    <textarea onChange={(event) => this.setState({ value: event.target.value })}
                                        style={{ width: '100%', borderTop: 'none', borderRight: 'none', borderLeft: 'none', lineHeight: 1 }}
                                        value={value} placeholder='Nội dung tin tức' rows='15'></textarea>
                                    <div style={{ marginBottom: '16px' }}></div>
                                    <MuiPickersUtilsProvider utils={MomentUtils}>
                                        <DatePicker
                                            ampm={false}
                                            label="Ngày hoạt động"
                                            format="DD-MM-YYYY"
                                            value={startActive}
                                            minDate={idUpdate ? '' : new Date()}
                                            onChange={(date) => this.setState({ startActive: date })}
                                            // disablePast
                                            leftArrowIcon={<KeyboardArrowLeft />}
                                            rightArrowIcon={<KeyboardArrowRight />}
                                            style={{ width: '100%' }}
                                        />
                                    </MuiPickersUtilsProvider>
                                </div>
                            </CardBody>
                            <CardFooter>
                                <button style={{ float: "right", marginLeft: "5px" }} onClick={() => { this.handleCancelData() }}>Hủy</button>
                                <button style={{ float: "right" }} onClick={() => { this.create() }}>{idUpdate ? 'Cập nhật' : 'Thêm mới'}</button>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    datePicker: {
        border: '1px solid',
        textAlign: 'center'
    },
    alertFadeIn: {
        height: '50px',
        lineHeight: '3',
        textAlign: 'left',
        marginLeft: '15px',
        fontWeight: 'bold'
    },
    button: {
        borderRadius: 25,
        margin: 5
    },
    iconSelected: {
        position: 'absolute',
        right: 5,
        top: -5
    },
    active: {
        border: '1px solid #f05673',
        color: '#f05673',
        fontWeight: 'bold'
    },
    inActive: {
        borderColor: '#4a4a4a',
        color: '#4a4a4a'
    }
});

export default withStyles(styles)(News);