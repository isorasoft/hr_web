import client from '../utils/client-utils';
import API from '../config/api';
export default {
    search(param, cb) {
        console.log(param)
        client.requestApi("get", API.contract_type.search, {}, (s, e) => {
            if (cb) {
                cb(s, e);
            }
        })
    },
    create(param, cb) {
        client.requestApi("POST", API.contract_type.create, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
    update(param, cb) {
        client.requestApi("PUT", API.contract_type.update + "/" + param.id, param, (s, e) => {
            if (cb)
                cb(s, e);
        })
    },
}